<?php

namespace App\Controller;

use App\Entity\Employee;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class EmployeeController extends AbstractController
{
	private ManagerRegistry $doctrine;

	public function __construct(ManagerRegistry $doctrine)
	{
		$this->doctrine = $doctrine;
	}

	#[Route('/employee', name: 'app_employee', methods: 'GET')]
	public function index(): Response
	{
		$employees = $this->doctrine
			->getRepository(Employee::class)
			->findAll();
		$data = [];
		foreach ($employees as $employee) {
			$data[] = [
				'id' => $employee->getId(),
				'fullname' => $employee->getFullname(),
				'email' => $employee->getEmail(),
				'password' => $employee->getPassword(),
				'degree' => $employee->getDegree(),
				'designation' => $employee->getDesignation(),
				'address' => $employee->getAddress(),
				'contact' => $employee->getContact(),
			];
		}
		return $this->json($data);
	}

	#[Route('/employee', name: 'add_employee', methods: 'POST')]
	public function addEmployee(Request $request): Response
	{
		$entityManager = $this->doctrine->getManager();

		$employee = new Employee();
		$employee->setFullname($request->get('fullname'));
		$employee->setEmail($request->get('email'));
		$employee->setPassword($request->get('password'));
		$employee->setDegree($request->get('degree'));
		$employee->setDesignation($request->get('designation'));
		$employee->setAddress($request->get('address'));
		$employee->setContact($request->get('contact'));

		$entityManager->persist($employee);
		$entityManager->flush();

		return $this->json('New Employee has been added successfully with id ' . $employee->getId());
	}

	#[Route('/employee/{id}', name: 'employee_show', methods: 'GET')]
	public function showEmployee(int $id): Response
	{
		$employee = $this->doctrine->getRepository(Employee::class)->find($id);

		if (!$employee) {
			return $this->json('No Employee found for id' . $id, 404);
		}

		$data = [
			'id' => $employee->getId(),
			'fullname' => $employee->getFullname(),
			'email' => $employee->getEmail(),
			'degree' => $employee->getDegree(),
			'designation' => $employee->getDesignation(),
			'address' => $employee->getAddress(),
			'contact' => $employee->getContact(),
			'password' => $employee->getPassword(),
		];

		return $this->json($data);
	}

	#[Route('/employee/{id}', name: 'employee_edit', methods: ['PUT', 'PATCH'])]
	public function editEmployee(Request $request, int $id): Response
	{
		$entityManager = $this->doctrine->getManager();
		$employee = $entityManager->getRepository(Employee::class)->find($id);

		if (!$employee) {
			return $this->json('No Employee found for id' . $id, 404);
		}

		try {
			$content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
		} catch (\JsonException $e) {
			dump($e);
		}

		$employee->setFullname($content->fullname);
		$employee->setEmail($content->email);
		$employee->setPassword($content->password);
		$employee->setDegree($content->degree);
		$employee->setDesignation($content->designation);
		$employee->setAddress($content->address);
		$employee->setContact($content->contact);
		$entityManager->flush();
		$data = [
			'id' => $employee->getId(),
			'name' => $employee->getFullname(),
			'password' => $employee->getPassword(),
			'email' => $employee->getEmail(),
			'degree' => $employee->getDegree(),
			'designation' => $employee->getDesignation(),
			'address' => $employee->getAddress(),
			'contact' => $employee->getContact(),
		];

		return $this->json($data);
	}

	#[Route('/employee/{id}', name: 'employee_delete', methods: 'DELETE')]
	public function delete(int $id): Response
	{
		$entityManager = $this->doctrine->getManager();
		$employee = $entityManager->getRepository(Employee::class)->find($id);

		if (!$employee) {
			return $this->json('No Employee found for id' . $id, 404);
		}

		$entityManager->remove($employee);
		$entityManager->flush();

		return $this->json('Deleted a Employee successfully with id ' . $id);
	}
}
