## Based on

https://www.twilio.com/blog/get-started-docker-symfony

## CLI

```
$ docker compose up -d --build

$ docker compose exec php symfony new .
```

### <YOUR_IP>:8585

## OTHER

```
$ docker compose exec php symfony <COMMAND>
or
$ docker compose exec php php /bin/console <COMMAND>
or
$ docker compose exec php composer <COMMAND>
```
